# BoB schema participantInfo

The purpose is to describe allowed properties for object participantInfo, specified in [participantMetadata.yml](https://bitbucket.org/samtrafiken/bob-api-participant-metadata/src/master/participantMetadata.yaml)

This is a part of the [BoB - National Ticket and Payment Standard](https://bob.samtrafiken.se), hosted by Samtrafiken i Sverige AB

